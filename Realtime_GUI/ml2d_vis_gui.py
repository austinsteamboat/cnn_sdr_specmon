import sys
import zmq
import time
import pickle
import ml_sdr_pb2
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import QThread, SIGNAL

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
import numpy as np
import matplotlib.pyplot as plt

# Load Labels
data_dir = "../ML_Classifiers/ML_Data/"
file_name = "test2"
lab_list_out = data_dir+file_name+"_lab_list"
with open(lab_list_out,'r') as labl_out:
    lab_list = pickle.load(labl_out)	

# Stop Flag
stop_flag = False
# Global Variables, because I'm lazy
seq_count = 0
col_scan_str = ["nthr","nbin","nvec","navg","nbat"] # All int
col_scan_val = [1,2048,20*16,50,1] # All int
rad_scan_str = ["rate","freq","bw","gain","ant"] # double, double, double, double, string
rad_scan_val = [20.0,617.0,16.0,40.0,"RX2"] # double, double, double, double, string
# Proto Buf Encoding
mlsdr = ml_sdr_pb2.ml_sdr_config_payload()

# Time Stamp
mlsdr.stamp.seq = 0
mlsdr.stamp.sec = 0
mlsdr.stamp.nsec = 0

# Radio config
mlsdr.rad_config.frequency = rad_scan_val[rad_scan_str.index("freq")]
mlsdr.rad_config.sample_rate = rad_scan_val[rad_scan_str.index("rate")]
mlsdr.rad_config.bw = rad_scan_val[rad_scan_str.index("bw")]
mlsdr.rad_config.gain = rad_scan_val[rad_scan_str.index("gain")]
mlsdr.rad_config.antenna = rad_scan_val[rad_scan_str.index("ant")]

# Col config
mlsdr.fecol_config.number_of_fft_threads = col_scan_val[col_scan_str.index("nthr")]
mlsdr.fecol_config.number_of_fft_bins = col_scan_val[col_scan_str.index("nbin")]
mlsdr.fecol_config.number_of_vectors = col_scan_val[col_scan_str.index("nvec")]
mlsdr.fecol_config.number_of_max_holds = col_scan_val[col_scan_str.index("navg")]
mlsdr.fecol_config.number_of_batches = col_scan_val[col_scan_str.index("nbat")]

# Global Variables for Plotting
dat_mat_ds = np.empty([0,0])
y_pred_out = np.empty([0,0])

# Paremeters of Data for Plotting
nbins = 2048
samp_rate = 20
ds = 16
nvecs=16
f_vec_ds = np.linspace(-samp_rate/2.0,samp_rate/2.0,nbins/ds)

# ZMQ Setup
context = zmq.Context()

#  Socket to talk to server
print("Connecting to RF spectrum data server")
socket = context.socket(zmq.REQ)
#ml_server = "localhost"
ml_server = "odroid.local"
socket.connect("tcp://"+ml_server+":5558")

# Thread for updating data  
class getDataThread(QThread):
    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def run(self):
        global ml_sdr
        global seq_count
        global dat_mat_ds
        global y_pred_out
        # Time Stamp
        mlsdr.stamp.seq = seq_count
        cur_time = time.time()
        mlsdr.stamp.sec = int(np.floor(cur_time))
        mlsdr.stamp.nsec = int((cur_time-np.floor(cur_time))*1e9)
        message_out = mlsdr.SerializeToString()
        seq_count = seq_count+1
        print("Requesting data...")
        socket.send(message_out)
        np_data_list_in = socket.recv_pyobj()
        dat_mat_ds = np_data_list_in[0]
        y_pred_out = np_data_list_in[1]


class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)
        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        
        self.get_data = getDataThread()
        self.connect(self.get_data,SIGNAL("finished()"),self.plot)
        # Buttons
        # Start Button?
        self.b1 = QtGui.QPushButton('Start')
        self.b1.setObjectName("start_butt")# couldn't resist
        self.b1.installEventFilter(self)

        # Stop Button
        self.b2 = QtGui.QPushButton('Stop')
        self.b2.setObjectName("stop_butt")
        self.b2.installEventFilter(self)

        # Text boxes
        # Num Batches
        self.tx1 = QtGui.QLineEdit()
        self.tx1.setObjectName("num_bat")
        self.tx1.setValidator(QtGui.QIntValidator())
        self.tx1.setText(str(mlsdr.fecol_config.number_of_vectors/nvecs))
        self.tx1.installEventFilter(self)
        self.tx1_lab = QtGui.QLabel()
        self.tx1_lab.setText("Number of Batches")
        
        # Num Avg
        self.tx2 = QtGui.QLineEdit()
        self.tx2.setObjectName("num_avg")
        self.tx2.setValidator(QtGui.QIntValidator())
        self.tx2.setText(str(mlsdr.fecol_config.number_of_max_holds))
        self.tx2.installEventFilter(self)
        self.tx2_lab = QtGui.QLabel()
        self.tx2_lab.setText("Number of Averages")
        
        # RX Gain
        self.tx3 = QtGui.QLineEdit()
        self.tx3.setObjectName("gain_val")
        self.tx3.setValidator(QtGui.QDoubleValidator())
        self.tx3.setText(str(mlsdr.rad_config.gain))
        self.tx3.installEventFilter(self)
        self.tx3_lab = QtGui.QLabel()
        self.tx3_lab.setText("RX Gain")

        # RX Freq
        self.tx4 = QtGui.QLineEdit()
        self.tx4.setObjectName("freq_val")
        self.tx4.setValidator(QtGui.QDoubleValidator())
        self.tx4.setText(str(mlsdr.rad_config.frequency))
        self.tx4.installEventFilter(self)
        self.tx4_lab = QtGui.QLabel()
        self.tx4_lab.setText("RX Freq")

        # set the layout
        layout = QtGui.QGridLayout()

        layout.addWidget(self.toolbar,0,0,1,3)
        layout.addWidget(self.canvas,1,0,1,3)
        layout.addWidget(self.tx1_lab,2,0)
        layout.addWidget(self.tx1,2,1)
        layout.addWidget(self.tx2_lab,3,0)
        layout.addWidget(self.tx2,3,1)
        layout.addWidget(self.tx3_lab,4,0)
        layout.addWidget(self.tx3,4,1)
        layout.addWidget(self.tx4_lab,5,0)
        layout.addWidget(self.tx4,5,1)
        layout.addWidget(self.b1,2,2)
        layout.addWidget(self.b2,3,2) 
        
        self.setLayout(layout)

    def eventFilter(self, object, event):
        global stop_flag
        if ((event.type() == QtCore.QEvent.MouseButtonPress) and isinstance(object,QtGui.QPushButton)):
            if(object.objectName() == self.b1.objectName()):
                self.get_data.start()
                stop_flag = False
                return True
            
            if(object.objectName() == self.b2.objectName()):
                self.get_data.quit() # or quit?
                stop_flag = True
                return True
                
            else:
                return True
            
        if ((event.type() == QtCore.QEvent.KeyPress) and isinstance(object,QtGui.QLineEdit)):
            if(event.key()==QtCore.Qt.Key_Return):
                if(object.objectName() == self.tx1.objectName()):
                    self.set_bat()
                    return True

                if(object.objectName() == self.tx2.objectName()):
                    self.set_avg()
                    return True                    

                if(object.objectName() == self.tx3.objectName()):
                    self.set_gain()
                    return True                    

                if(object.objectName() == self.tx4.objectName()):
                    self.set_freq()
                    return True                
                return True
            
        return False

    def enterPress(self):
        text = self.tx1.text()
        print "contents of text box: "+text

    def set_bat(self):
        global col_scan_str
        global col_scan_val
        global col_file
        bat_ind = col_scan_str.index("nvec")
        val = self.tx1.text()
        print("Set number of batches to: %d"%int(val))
        val = int(val)*nvecs
        col_scan_val[bat_ind] = val
        mlsdr.fecol_config.number_of_vectors = col_scan_val[col_scan_str.index("nvec")]

    def set_avg(self):
        global col_scan_str
        global col_scan_val
        global col_file
        avg_ind = col_scan_str.index("navg")
        val = self.tx2.text()
        val = int(val)
        col_scan_val[avg_ind] = val
        mlsdr.fecol_config.number_of_max_holds = col_scan_val[col_scan_str.index("navg")]
        print("Set number of avg. to: %d"%val)

    def set_gain(self):
        global rad_scan_str
        global rad_scan_val
        global rad_file
        gain_ind = rad_scan_str.index("gain")
        val = self.tx3.text()
        val = float(val)
        rad_scan_val[gain_ind] = val
        mlsdr.rad_config.gain = rad_scan_val[rad_scan_str.index("gain")]
        print("Retuned Gain to: %f"%val)

    def set_freq(self):
        global rad_scan_str
        global rad_scan_val
        global rad_file
        gain_ind = rad_scan_str.index("freq")
        val = self.tx4.text()
        val = float(val)
        rad_scan_val[gain_ind] = val
        mlsdr.rad_config.frequency = rad_scan_val[rad_scan_str.index("freq")]
        print("Retuned Freq to: %f"%val)
        
    def plot(self):
        # create an axis
        ax0 = self.figure.add_subplot(221)
        # discards the old graph
        ax0.hold(False)
        # plot data
        ax0.plot(f_vec_ds,np.transpose(dat_mat_ds))
        ax0.set_xlabel("Frequency in MHz")
        ax0.set_ylabel("Power in unscaled dB")
        ax0.set_title("Raw Data In")        
        
        # create an axis
        ax1 = self.figure.add_subplot(222)
        # discards the old graph
        ax1.hold(False)
        # plot data
        ax1.imshow(dat_mat_ds)
        ax1.set_ylabel("Frequency Index")
        ax1.set_xlabel("Data Vector Index")
        ax1.set_title("Waterfall Plot")
        ax1.set_aspect('auto')
        
        # create an axis
        ax2 = self.figure.add_subplot(223)
        # discards the old graph
        ax2.hold(False)
        # plot data
        ax2.plot(y_pred_out)
        ax2.set_xlabel("Test Data Index")
        ax2.set_ylabel("Softmax Output")
        ax2.set_ylim([-0.1,1.1])       
        ax2.legend(lab_list,loc=6)
        # create an axis
        ax3 = self.figure.add_subplot(224)
        # discards the old graph
        ax3.hold(False)
        # plot data
        bar_tic = np.arange(5)
        width = 0.5
        nvecs_norm = 1.0/(mlsdr.fecol_config.number_of_vectors/nvecs)
        ax3.bar(bar_tic,np.sum(y_pred_out,axis=0)*nvecs_norm,width)
        ax3.set_xticks(bar_tic+width/2)
        ax3.set_xticklabels(lab_list)
        ax3.set_ylim([0.0,1.05])
        ax3.set_ylabel("Normalized Sum Over the Set")

        # refresh canvas
        self.canvas.draw()
        if(not(stop_flag)):
            self.get_data.start()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())
