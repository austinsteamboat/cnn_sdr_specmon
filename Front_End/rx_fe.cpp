#include <rx_fe.hpp>

static const double pi= double(std::acos(-1.0));

// Generate FFT window: Blackman Harris with FFT shift scaling
double rx_fe::window_maker_bh(std::vector<double>* win_vec, size_t nsamps){
	size_t n = 0;
	double w_n;
	double win_pwr = 0;
	for(n = 0; n<nsamps; n++){
    	w_n = 0.35875 //blackman-harris window
    	      -0.48829*std::cos(2*pi*n/(nsamps-1))
              +0.14128*std::cos(4*pi*n/(nsamps-1))
              -0.01168*std::cos(6*pi*n/(nsamps-1));
        win_pwr += sqrt(w_n*w_n);	
       	win_vec->at(n) = w_n*pow(-1,n);
    }
    win_pwr = win_pwr/nsamps;
    return win_pwr;// Return the windows power
}

// Actual Windowing Multiplication pre-fft
void rx_fe::window_funct(std::vector<std::complex<double> >* time_vec, std::vector<double>* win_vec,int offset_ind){
	size_t nsamps = win_vec -> size();
	size_t n = 0;
	size_t offset_n = 0;
	double w_n;
	double win_pwr = 0;
	for(n = 0; n<nsamps; n++){
	    offset_n = n+size_t(offset_ind);
       	time_vec->at(offset_n) = time_vec->at(offset_n)*win_vec->at(n);
    }
}

// FFTw Calls
void rx_fe::fft_funct(fftw_plan plan, size_t nbins, fftw_complex* fft_vec, std::vector<std::complex<double> >& samp_buff, int offset_ind){
		memcpy(&fft_vec[0],&samp_buff[offset_ind],nbins*sizeof(fftw_complex));  
		fftw_execute(plan);
}

// Convert from complex vector to dB power vector, leverage log2 for improved performance
void rx_fe::comp2db(std::vector<double>* pow_vec, fftw_complex* fft_vec, double win_pwr){
	size_t nsamps = pow_vec -> size();
	size_t n;
	double pow_add = -20*std::log10(nsamps)-10*std::log10(win_pwr)+3;
	double pow_tmp;
	for(n=0; n<nsamps; n++){
		 pow_tmp = 0.30103*10*std::log2(fft_vec[n][0]*fft_vec[n][0]+fft_vec[n][1]*fft_vec[n][1])+pow_add;
		if(pow_tmp > (pow_vec->at(n))){
			pow_vec->at(n) = pow_tmp;
		}
	}
}

