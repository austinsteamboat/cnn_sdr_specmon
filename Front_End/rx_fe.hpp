#include <iostream>
#include <stdlib.h>
#include <fftw3.h>
#include <complex.h>
#include <math.h>
#include <vector>
#include <memory.h>

// Class for IQ to dB spectrum with max hold front-end
class rx_fe
{
public:

	// Generate FFT window: Blackman Harris with FFT shift scaling
	double window_maker_bh(std::vector<double>*, size_t);

	// Actual Windowing Multiplication pre-fft
	void window_funct(std::vector<std::complex<double> >*, std::vector<double>*, int);

	// FFTw Calls
	void fft_funct(fftw_plan, size_t, fftw_complex*, 
				   std::vector<std::complex<double> >&, int);
				   
	// Convert from complex vector to dB power vector, leverage log2 for improved performance
	void comp2db(std::vector<double>*, fftw_complex*, double);
};
	
