// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <uhd/types/tune_request.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/transport/udp_simple.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <zmq.hpp>
#include <rx_fe.hpp> // Includes stdlib, fftw3, complex, math
#include <chrono>
#include <ctime>
#include "ml_sdr.pb.h"

namespace po = boost::program_options;

// Main
int UHD_SAFE_MAIN(int argc, char *argv[]){
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    // UHD Thread Setup
    uhd::set_thread_priority_safe();

    bool verbose = true;

    // ZeroMQ RF data Reply Server Setup, Bind to tcp://localhost:5557
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    printf("Starting RF reply server on tcp://localhost:5557...\n");
    socket.bind ("tcp://*:5557");

    // ZeroMQ Protobuf Reply Server Setup, Bind to tcp://localhost:5559
    zmq::context_t context_pb (1);
    zmq::socket_t socket_pb (context_pb, ZMQ_REP);
    printf("Starting PB publisher on tcp://localhost:5559...\n");
    socket_pb.bind ("tcp://*:5559");
    
    // Front End Processing Object
    rx_fe rxfe_proc;

    // Protobuf Global
    ml_sdr_proto::ml_sdr_config_payload mlsdr;
    int pb_size_out;
    std::string pb_msg_out;
    // Time Structs
    std::chrono::time_point<std::chrono::high_resolution_clock> time_val;
    std::chrono::duration<double> time_double;
    double dub_val;
    unsigned int seq_val = 0; 
    unsigned int sec_val;
    unsigned int nan_val;

    //variables to be set by po
    std::string args, file, ant, subdev, ref;

    // Radio Characteristics 
    double rate, freq, gain, bw;
    ref = "internal";
    // User Config, Default:
    rate = 20.0;
    freq = 617;  
    bw = 0.8*rate;
    gain = 40;
    ant = "RX2";
    size_t num_avg = 300;
    rate = rate*1e6;
    freq = freq*1e6;
    bw = bw*1e6;
    bool reset_flag = false;
    // If we want a non-standard image:
    //args = "fpga=/usr/local/share/uhd/images/usrp_b200_fpga_test.bin";
    args = "master_clock_rate=20e6";
    // Collection Setup
    // Defaults
    int nthreads = 1; // Number of threads
    size_t num_bins = 2048; //Number of FFT bins
    size_t num_vecs = 500; // Number of Max Hold Vectors to assemble 
    
    // FFTW setup
    //fftw_init_threads();
    //fftw_plan_with_nthreads(nthreads);
    fftw_complex in[num_bins], out[num_bins];
    fftw_plan p;
    p = fftw_plan_dft_1d(num_bins, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    // Max Hold and Buffer setup

    
    // Output Array
    std::vector<double> pow_out(num_bins,-1000.0);
    std::vector<double> win_tmp(num_bins,0.0);
    
    // Build Windowing vector
    double win_pwr;
    win_pwr = rxfe_proc.window_maker_bh(&win_tmp, num_bins);

    start_loop:
    //=========Create and Configure UHD Device==========
    //create a usrp device
    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);
    //Lock mboard clocks
    usrp->set_clock_source(ref);
    //setup streaming
    uhd::rx_metadata_t md;
    uhd::stream_args_t stream_args("fc64"); //complex floats
    //create a receive streamer
    uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args);
    // Timeout value for data acquisition     
    double timeout = 3.0;
    // ZMQ Server Loop
    std::vector<std::string> rx_names = usrp->get_rx_sensor_names();
    printf("Sensor Names are: \n");
    for(int kk=0;kk<rx_names.size();++kk){
        std::string rx_1 = rx_names[kk];
        printf("%s \n",rx_1.data());
    }
    if(reset_flag){
        reset_flag = false;
        goto data_loop;
    }
    
    zmq_loop:
    while(true){
        zmq::message_t request;
        socket.recv (&request);
        // Protobuf Decoding
        ml_sdr_proto::timestamp stamp;
        ml_sdr_proto::b200_config rad_config;
        ml_sdr_proto::front_end_collect_config col_config;
        std::string msg_str(static_cast<char*>(request.data()), request.size());
        mlsdr.ParseFromString(msg_str);
        stamp = mlsdr.stamp();
        rad_config = mlsdr.rad_config();
        col_config = mlsdr.fecol_config();
        if (verbose) {
            printf("Request Received \n");
        }
        // Radio Config:
        rate = rad_config.sample_rate();
        rate = rate*1e6;
        freq = rad_config.frequency();
        freq = freq*1e6;
        bw = rad_config.bw();
        bw = bw*1e6;
        if ( rate < 1 ) {
            fprintf(stderr,"rate=%f, freq=%f, bw=%f\n", rate, freq, bw);
            fprintf(stderr,"Retrying...\n");
            continue;
        }
        gain = rad_config.gain();
        ant = rad_config.antenna();
        // Collection Config:
        num_avg = size_t(col_config.number_of_max_holds());
        num_vecs = size_t(col_config.number_of_vectors());
        // Get and Set Time Stamps 
        ++seq_val;
        time_val = std::chrono::high_resolution_clock::now();
        time_double = time_val.time_since_epoch();
        dub_val = time_double.count();
        sec_val = floor(dub_val);
        nan_val = round((dub_val-sec_val)*1e9);
        stamp.set_seq(seq_val);
        stamp.set_sec(sec_val);
        stamp.set_nsec(nan_val);
        mlsdr.mutable_stamp()->CopyFrom(stamp);
        pb_size_out = mlsdr.ByteSize();
        goto data_loop;
    }
    
    // Loop for taking data:
    data_loop:
    
    //Memory Array Allocatoin 
    double * mat_out = new double[num_vecs*num_bins];
    std::vector<std::complex<double> > buff(num_bins);
    zmq::message_t m_out(num_bins*num_vecs*sizeof(double));
    zmq::message_t mpb_out(pb_size_out);

    //set the rx sample rate
    usrp->set_rx_rate(rate);

    //set the rx center frequency
    uhd::tune_request_t tune_request(freq);
    usrp->set_rx_freq(tune_request);
    //set the rx rf gain
    usrp->set_rx_gain(gain);

    //set the analog frontend filter bandwidth
    usrp->set_rx_bandwidth(bw);

    //set the antenna
    usrp->set_rx_antenna(ant);
    
    // Build stream command to request a full data packet 
    uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE);
    stream_cmd.num_samps = size_t(buff.size());
    stream_cmd.stream_now = true;
    
    // Loop until we've gotten all vectors 
    for(int jj=0; jj<int(num_vecs); ++jj){
		// Loop until we get all averages
        for(int ii=0; ii<int(num_avg); ++ii){
            // Stream call: get nbins
		    rx_stream->issue_stream_cmd(stream_cmd);
		    size_t num_rx_samps = rx_stream->recv(&buff.front(), buff.size(), md);

		    //handle an error
		    switch(md.error_code){
		    case uhd::rx_metadata_t::ERROR_CODE_NONE:
		        break;

		    case uhd::rx_metadata_t::ERROR_CODE_TIMEOUT:
		        if (num_rx_samps == 0) continue;
		        printf("Got timeout exiting loop...\n");
		        goto done_loop;
		        
		    case uhd::rx_metadata_t::ERROR_CODE_OVERFLOW:
		        printf("WARNING: Got an overflow, restarting...\n");
		        rx_stream->issue_stream_cmd(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
		        reset_flag = true;
		        goto start_loop;    

		    default:
		        printf("Got error code 0x%x, exiting loop...\n",md.error_code);
		        goto done_loop;
		    }
		    
            // Run FFT calls:
            // Run window function
            rxfe_proc.window_funct(&buff,&win_tmp,0);
            // Run FFT
            rxfe_proc.fft_funct(p,num_bins,in,buff,0);
            // Convert Complex FFT to dB power and run max hold
            rxfe_proc.comp2db(&pow_out, out, win_pwr);  
        }
        // Populate Output Array
        memcpy(&mat_out[jj*num_bins],&pow_out[0],num_bins*sizeof(double));
        pow_out.assign(num_bins,-1000.0);// Reset working array
    }

    // Once We're Done with the number of vectors, send the ZMQ results
    if ( verbose ) {
        printf("Sending ZMQ!\n"); 
    }
    memcpy(m_out.data(),mat_out,num_bins*num_vecs*sizeof(double));
    
    // Send RF Data Response
    socket.send(m_out);

    // Wait for request for pb data
    while(true){
        zmq::message_t request_pb;
        socket_pb.recv (&request_pb);
        // Load response
        mlsdr.SerializeToString(&pb_msg_out);
        memcpy(mpb_out.data(),pb_msg_out.data(),pb_size_out);
        socket_pb.send(mpb_out);
        break;
    }
    
    // Clean up buffers 
    delete [] mat_out;

    // Loop back to the ZMQ section 
    goto zmq_loop;
    
    // If we get an error or we're actually done got ot termination 
    done_loop:

    // Terminate Loop
    printf("Terminating!\n");
    // Stop UHD Streaming     
    rx_stream->issue_stream_cmd(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
    // Clean up FFTw plans
    //fftw_cleanup_threads();
    fftw_destroy_plan(p); 
    return EXIT_SUCCESS;
}
