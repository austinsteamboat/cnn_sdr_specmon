This repository contains files for running a CNN based, realtime spectrum classifier. 

Directory Descriptions:
-----------------------
1. Front End
    Contains all the files for interfacing with UHD, transforming to frequency domain, and performing max hold on the power spectral densities. Front End is written in C++ for optimal performance. Functions for performing max hold and PSD transform are encapsulated in a custom class. Interface to and from Front End is accomplished using ZeroMQ sockets. Raw spectrum data is sent as binary values. Requests for data include radio configuration settings. Requests are serealized using a proto-buffer structure. The same front-end is used for realtime classification as well as data labelling. 
    
2. ML Classifiers
   This directory contains files relevant to the machine learning classifiers. Currently, a 2D CNN is implemented to perform real-time classification. The tensorflow architecture is used to implement the CNN. A small and large CNN have been trained. Training weights are included in the ML Data subdirectory. This classifiers is currently used only in the realtime system. It receives a request from the GUI via ZeroMQ, forwards the request to the front end which supplies a set of PSD's. The classifier performs a classification and responds to the GUI's original request with the probability of each signal class. 
   
3. Protobuf Files
    This directory contains the files that define the protobuf structure used throughout. They also contain the Makefile necessary to generate new definitions. The basic structure includes timestamps as a required field with radio configuration and INS/GPS information as optional fields. USERS NEED TO RUN THE MAKEFILE IN THIS DIRECTORY FIRST! This is to populate the proper protobuf headers for the dependent directories. 
    
4. Realtime GUI
    This contains the realtime GUI. It's implemented in Qt and lets the user specify radio configuration for running the classifiers. It uses the same ZeroMQ request/response architecture and protobuf serialization used throughout the system. The realtime GUI interfaces with the realtime ML classifier which in turn interfaces with the front end to supply both data and classification predictions.
    
5. Data Labelling
    This contains the data labelling GUI. It interfaces directly to the front end to generate PSDs. This GUI can be used to quickly generate new data sets. The Qt GUI supports control using either a mouse and buttons or a faster keyboard shortcut approach. 
    
6. ODroid Tensorflow
    This directory contains the compiled ODroid wheel for installing tensorflow on an ODroid Xu4. Kind of the odd man out, but it took a long time to generate that wheel so I figure someone will find this useful. 
    
7. CNN Train and Eval
    This directory contains the tensorflow python scripts for re-training the 2D CNN. Labelled input data is required. This can be generated using the Data Labelling GUI or loading pre-labelled data. Currently, the labelled data sets we have are too large to practically share using git. A distribution system for our labelled data is forthcomming. 
    
Dependency Versions and Compatability:
--------------------------------------
Protobuffers
Syntax is version 2
Using libprotoc 2.5.0

This seems to be a sensitive dependency. If you re-make the headers for your target build you ought to be ok. 

UHD  
UHD_003.010.git-156-g2d68f228

The UHD interface is very basic, it should be compatible with a wide variety of versions, but this has not been tested. 

Tensorflow
git hash: 172e36e7b50aedb1470934e672c9810bb2400a04
For ODroid see included wheel file.

Similarly, the tensorflow version probably has wide compatability, but this is not confirmed. 

Qt
qt4

FFTW
Version 3

