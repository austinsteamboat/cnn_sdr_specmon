import zmq
import time
import pickle
import struct
import ml_sdr_pb2
import sys, getopt
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# Arg Parsing

def get_args(argv):
    FE_server_name = 'localhost'
    GUI_client_name = '*'
    BUILD_BIG_CNN = False
    try:
       opts, args = getopt.getopt(argv,'x',['help','FE-server=','GUI-client=','big-cnn'])
    except getopt.GetoptError:
       print 'ml_cnn2d_rt.py --FE-server <FE Server Hostname> --GUI-client <GUI Client Hostname> --big-cnn'
       sys.exit(2)
    for opt, arg in opts:
        if opt == '--help':
            print 'ml_cnn2d_rt.py --FE-server <FE Server Hostname> --GUI-client <GUI Client Hostname> --big-cnn'
            sys.exit()
        elif opt in ("--FE-server"):
            FE_server_name = arg
        elif opt in ("--GUI-client"):
            GUI_client_name = arg
        elif opt in ("--big-cnn"):
            BUILD_BIG_CNN = True
    return FE_server_name, GUI_client_name, BUILD_BIG_CNN

FE_name,GUI_name,BIG_Flag = get_args(sys.argv[1:])

#  ZMQ Setup
context = zmq.Context()

#  ZMQ GUI Reply
print("Connecting to GUI on tcp://"+GUI_name+":5558...")
GUI_rep_socket = context.socket(zmq.REP)
GUI_rep_socket.bind("tcp://"+GUI_name+":5558")

#  ZMQ RxFE Request
print("Connecting to Rx FE on tcp://"+FE_name+":5557...");
RF_req_socket = context.socket(zmq.REQ)
RF_req_socket.connect("tcp://"+FE_name+":5557")

#  ZMQ Protobuf Subscriber
print("Connecting to ProtoBuf FE on tcp://"+FE_name+":5559...");
PB_sub_socket = context.socket(zmq.REQ)
PB_sub_socket.connect("tcp://"+FE_name+":5559")

# Model Params:
# Layer Variables
# Input
nbins = 2048
ds = 16
nvecs = 16

if(BIG_Flag):
    # Big
    print("Building Big CNN Network")
    # ReLU1
    c1 = 7
    h1 = 2
    n1 = 32
    # ReLU2
    c2 = 5
    h2 = 2
    n2 = 64
    # FC1
    nfc1 = 2048
    check_file = "ML_Data/2D_CNN_big.ckpt"
else:
    # Small
    print("Building Small CNN Network")
    # ReLU1
    c1 = 5
    h1 = 2
    n1 = 16
    # ReLU2
    c2 = 3
    h2 = 2
    n2 = 32
    # FC1
    nfc1 = 1024
    check_file = "ML_Data/2D_CNN_small.ckpt"

# FC2
flen = 5
nfc2 = flen

sess = tf.InteractiveSession()

# ML Setup
# Helper Functions
def weight_variable(shape):
  initial = tf.constant(0.0, shape=shape)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.0, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_nxn(x,n):
  return tf.nn.max_pool(x, ksize=[1, n, n, 1],
                        strides=[1, n, n, 1], padding='SAME')                                            

def max_pool_1xn(x,n):
  return tf.nn.max_pool(x, ksize=[1, 1, n, 1],
                        strides=[1, 1, n, 1], padding='SAME') 

# Input/output placeholders
x = tf.placeholder(tf.float32, shape=[None, nbins*nvecs])
y_ = tf.placeholder(tf.float32, shape=[None, flen])

# Input Layer
x_image = tf.reshape(x, [-1,nvecs,nbins,1])

# First Layer: ReLU
with tf.name_scope("ReLU_1") as scope:
  pool_in = max_pool_1xn(x_image,ds)
  W_conv1 = weight_variable([c1, c1, 1, n1])
  b_conv1 = bias_variable([n1])
  h_conv1 = tf.nn.relu(conv2d(pool_in, W_conv1) + b_conv1)
  h_pool1 = max_pool_nxn(h_conv1,h1)

# Second Layer: ReLU
with tf.name_scope("ReLU_2") as scope:
  W_conv2 = weight_variable([c2, c2, n1, n2])
  b_conv2 = bias_variable([n2])
  h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
  h_pool2 = max_pool_nxn(h_conv2,h2)

# Third Layer: Dense
with tf.name_scope("Dense_1") as scope:
  W_fc1 = weight_variable([nbins/ds*nvecs/((h1*h1)*(h2*h2))*n2, nfc1])
  b_fc1 = bias_variable([nfc1])
  h_pool2_flat = tf.reshape(h_pool2, [-1, nbins/ds*nvecs/((h1*h1)*(h2*h2))*n2])
  h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
  keep_prob = tf.placeholder(tf.float32)
  h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# Readout Layer
with tf.name_scope("Readout_Layer") as scope:
  W_fc2 = weight_variable([nfc1, nfc2])
  b_fc2 = bias_variable([nfc2])
  y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2,name="y_conv")
  

# Initialize the Model
init = tf.initialize_all_variables().run()
saver = tf.train.Saver()
saver.restore(sess, check_file)
print("CNN Model restored!")

while(True):
    # Receive GUI Request
    message_GUI = GUI_rep_socket.recv()
    # Pass GUI Request to RF Front End
    RF_req_socket.send(message_GUI)
    # Recieve RF Data
    message_RF = RF_req_socket.recv_multipart()
    # Recieve Protobuf Response
    PB_sub_socket.send(b"Gimme")
    message_PB = PB_sub_socket.recv()
    mlsdr = ml_sdr_pb2.ml_sdr_config_payload()
    mlsdr.ParseFromString(message_PB)
    print "Seq: "+str(mlsdr.stamp.seq)
    print "Sec: "+str(mlsdr.stamp.sec)
    print "NSec: "+str(mlsdr.stamp.nsec)
    dat_mat = np.frombuffer(message_RF[0],dtype = np.dtype('float64'))
    # Offset minimum to 0 dBm, this is very important for convergence and allows us to train on SNR instead of raw dB offsets
    # Scaling factor was 0.01 as dynamic range was assumed to be ~ 100 this gives us a range from ~0.0-1.0
    # Scaling factor isn't as important for convergence, but 0'ing the dBm is very important 
    dat_mat = (dat_mat-np.min(dat_mat))*0.01
    nsamps = dat_mat.size/(nbins*nvecs)
    dat_mat = np.reshape(dat_mat,[nsamps,nbins*nvecs],order = 'C')
    y_pred_out = sess.run(y_conv,feed_dict={x: dat_mat, keep_prob: 1.0})
    data_ds = sess.run(pool_in,feed_dict={x: dat_mat})
    data_ds = np.reshape(data_ds,[nsamps*nvecs,nbins/ds])
    print("Sending ZMQ!")
    list_out = [data_ds,y_pred_out]
    GUI_rep_socket.send_pyobj(list_out)






