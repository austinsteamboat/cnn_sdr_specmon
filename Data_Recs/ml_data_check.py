import sys
import zmq
import time, datetime
import pickle
import struct
import ml_sdr_pb2
import numpy as np
from time import gmtime, strftime

def getInt(fd):
    sz = fd.read(4)
    if sz == '':
        return sz
    bytes = struct.unpack('i', sz)[0]
    return bytes

def processFile(filename):
    fd = open(filename, "rb")
    while fd:
        bytes = getInt(fd)
        if bytes == '':
            break
        message_PB = fd.read(bytes)

        num_rf = getInt(fd)
        message_RF = []
        for i in range(num_rf):
            message_RF.append( fd.read(getInt(fd)) )

        mlsdr = ml_sdr_pb2.ml_sdr_config_payload()
	mlsdr.ParseFromString(message_PB)
        print "PB is ", len(message_PB), "bytes long",  "RF has ", len(message_RF), "entries of size", len(message_RF[0])
	print "Seq: ", mlsdr.stamp.seq, "Sec: ", mlsdr.stamp.sec, "NSec: ", mlsdr.stamp.nsec
        print "Center", mlsdr.rad_config.frequency, \
            "rate", mlsdr.rad_config.sample_rate, \
            "bw", mlsdr.rad_config.bw, \
            "gain", mlsdr.rad_config.gain, \
            "ant", mlsdr.rad_config.antenna

        nbins = mlsdr.fecol_config.number_of_fft_bins
        nvecs = mlsdr.fecol_config.number_of_vectors

	dat_mat = np.frombuffer(message_RF[0],dtype = np.dtype('float64'))
	dat_mat = (dat_mat-np.min(dat_mat))*0.01
	nsamps = dat_mat.size/(nbins*nvecs)
	dat_mat = np.reshape(dat_mat,[nsamps,nbins*nvecs],order = 'C')

for file in sys.argv[1:]:
    print "Process ", file
    processFile(file)
