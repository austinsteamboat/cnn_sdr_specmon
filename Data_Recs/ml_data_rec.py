import sys
import zmq
import time, datetime
import pickle
import struct
import ml_sdr_pb2
import numpy as np
from time import gmtime, strftime

#  ZMQ Setup
context = zmq.Context()

##
## output file routines

PROTOBUF_FILE_OUTPUT_DIR = "/srv/scans"
PROTOBUF_FILE_OUTPUT_PREFIX = "dead"
PROTOBUF_FILE_OUTPUT_SUFFIX = ".proto"

def createProtobuf():
    timestamp = strftime ("%Y-%m-%d-%H-%M-%S", gmtime());
    filename = PROTOBUF_FILE_OUTPUT_DIR \
               + "/" \
               + PROTOBUF_FILE_OUTPUT_PREFIX \
               + "-proto-" + timestamp \
               + PROTOBUF_FILE_OUTPUT_SUFFIX
    print "Writing to new file", filename
    fd = open(filename, "wb")
    return fd
    

outputProtoBuf = None

# Initialize ZMQ Clients
#server_name = "remoteip"
server_name = "*"
#server_name = "odroid"
#server_name = "ettus-e3xx-sg1.local"

#  ZMQ RxFE Request
print("Connecting to Rx FE on tcp://"+server_name+":5557...");
RF_req_socket = context.socket(zmq.REQ)
RF_req_socket.bind("tcp://"+server_name+":5557")

#  ZMQ Protobuf Subscriber
print("Connecting to ProtoBuf FE on tcp://"+server_name+":5559...");
PB_sub_socket = context.socket(zmq.REQ)
PB_sub_socket.bind("tcp://"+server_name+":5559")

# Scan Tables

def mhz(x):
    return x * 1000000.0

atscScan = [
	{ 'freq' : mhz(473), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(497), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(503), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(509), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(515), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(521), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(527), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(533), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(539), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(557), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(563), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(569), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(581), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(587), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(593), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(598), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(617), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(623), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(641), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(647), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(653), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(671), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(677), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(683), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(689), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 },
	{ 'freq' : mhz(695), 'batch' : 100, 'avg' : 10, 'dwell':10, 'sleepAfter': 0.5 }
]

# Collection Settings 
col_scan_str = ["nthr","nbin","nvec","navg","nbat"] # All int
col_scan_val = [1,2048,20*16,50,1] # All int
rad_scan_str = ["rate","freq","bw","gain","ant"] # double, double, double, double, string
rad_scan_val = [20.0,617.0,16.0,40.0,"RX2"] # double, double, double, double, string
# Proto Buf Encoding
mlsdr = ml_sdr_pb2.ml_sdr_config_payload()

# Time Stamp
mlsdr.stamp.seq = 0
mlsdr.stamp.sec = 0
mlsdr.stamp.nsec = 0

# Radio config
mlsdr.rad_config.frequency = rad_scan_val[rad_scan_str.index("freq")]
mlsdr.rad_config.sample_rate = rad_scan_val[rad_scan_str.index("rate")]
mlsdr.rad_config.bw = rad_scan_val[rad_scan_str.index("bw")]
mlsdr.rad_config.gain = rad_scan_val[rad_scan_str.index("gain")]
mlsdr.rad_config.antenna = rad_scan_val[rad_scan_str.index("ant")]

# Col config
mlsdr.fecol_config.number_of_fft_threads = col_scan_val[col_scan_str.index("nthr")]
mlsdr.fecol_config.number_of_fft_bins = col_scan_val[col_scan_str.index("nbin")]
mlsdr.fecol_config.number_of_vectors = col_scan_val[col_scan_str.index("nvec")]
mlsdr.fecol_config.number_of_max_holds = col_scan_val[col_scan_str.index("navg")]
mlsdr.fecol_config.number_of_batches = col_scan_val[col_scan_str.index("nbat")]

scan_count = 0
dwell_count = 0
sleep_flag = False
while(True):
	scan_ind = np.mod(scan_count,len(atscScan))
	# Build protobuf request
	mlsdr.rad_config.frequency = atscScan[scan_ind]['freq']
	mlsdr.fecol_config.number_of_vectors = atscScan[scan_ind]['batch']
	mlsdr.fecol_config.number_of_max_holds = atscScan[scan_ind]['avg']
	message_out = mlsdr.SerializeToString()
	
	if(sleep_flag):
		time.sleep(atscScan[scan_ind]['sleepAfter'])
		sleep_flag = False
	
	# Request data with protobuf setting
	print("Requesting data:", \
              mlsdr.rad_config.sample_rate, \
              mlsdr.rad_config.frequency, \
              mlsdr.fecol_config.number_of_vectors, \
              mlsdr.fecol_config.number_of_max_holds )
	
	# Pass Protobuf Request to RF Front End
	RF_req_socket.send(message_out)
	
	# Recieve RF Data Response
	message_RF = RF_req_socket.recv_multipart()
	
        # Request protobuf populated values
        PB_sub_socket.send(b"Gimme")

	# Receive Protobuf response
	message_PB = PB_sub_socket.recv()
	
	mlsdr = ml_sdr_pb2.ml_sdr_config_payload()
	mlsdr.ParseFromString(message_PB)
	print "Seq: "+str(mlsdr.stamp.seq)
	print "Sec: "+str(mlsdr.stamp.sec)
	print "NSec: "+str(mlsdr.stamp.nsec)
        nbins = mlsdr.fecol_config.number_of_fft_bins
        nvecs = mlsdr.fecol_config.number_of_vectors

	dat_mat = np.frombuffer(message_RF[0],dtype = np.dtype('float64'))
	dat_mat = (dat_mat-np.min(dat_mat))*0.01
	nsamps = dat_mat.size/(nbins*nvecs)
	dat_mat = np.reshape(dat_mat,[nsamps,nbins*nvecs],order = 'C')
	
	print("Got some Data!")
	dwell_count += 1
	if(dwell_count>=atscScan[scan_ind]['dwell']):
		dwell_count = 0
		sleep_flag = True
	        scan_count += 1

        if outputProtoBuf == None:
            outputProtoBuf = createProtobuf()
        outputProtoBuf.write(struct.pack('i', len(message_PB)))
        outputProtoBuf.write(message_PB)
        outputProtoBuf.write(struct.pack('i', len(message_RF)))
        for scan in message_RF:
            outputProtoBuf.write(struct.pack('i', len(scan)))
            outputProtoBuf.write(scan)
        outputProtoBuf.flush()
        #
        # Cut it if it's bigger than a gb
        #
        if outputProtoBuf.tell() > ( 1024 **3 ):
            outputProtoBuf.close()
            outputProtoBuf = None





