import sys
import zmq
import time
import pickle
import ml_sdr_pb2
from PyQt4 import QtGui
from PyQt4 import QtCore

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt



# Global Variables, because I'm lazy
seq_count = 0
lab_list = []
col_scan_str = ["nthr","nbin","nvec","navg","nbat"] # All int
col_scan_val = [1,2048,16,100,1] # All int
rad_scan_str = ["rate","freq","bw","gain","ant"] # double, double, double, double, string
rad_scan_val = [20.0,617.0,16.0,40.0,"RX2"] # double, double, double, double, string
col_file = "col_conf.txt"
rad_file = "radio_conf.txt"
data_dir = "Data_Files/ML_Data/"
# Proto Buf Encoding
mlsdr = ml_sdr_pb2.ml_sdr_config_payload()

# Time Stamp
mlsdr.stamp.seq = 0
mlsdr.stamp.sec = 0
mlsdr.stamp.nsec = 0

# Radio config
mlsdr.rad_config.frequency = rad_scan_val[rad_scan_str.index("freq")]
mlsdr.rad_config.sample_rate = rad_scan_val[rad_scan_str.index("rate")]
mlsdr.rad_config.bw = rad_scan_val[rad_scan_str.index("bw")]
mlsdr.rad_config.gain = rad_scan_val[rad_scan_str.index("gain")]
mlsdr.rad_config.antenna = rad_scan_val[rad_scan_str.index("ant")]

# Col config
mlsdr.fecol_config.number_of_fft_threads = col_scan_val[col_scan_str.index("nthr")]
mlsdr.fecol_config.number_of_fft_bins = col_scan_val[col_scan_str.index("nbin")]
mlsdr.fecol_config.number_of_vectors = col_scan_val[col_scan_str.index("nvec")]
mlsdr.fecol_config.number_of_max_holds = col_scan_val[col_scan_str.index("navg")]
mlsdr.fecol_config.number_of_batches = col_scan_val[col_scan_str.index("nbat")]

dat_arr = np.empty([0,0]) # [number of data examples x size of an example]
lab_arr = np.empty([0,0]) # [number of data examples x number of signal types]
dat_mat_flat = np.empty([0,0])

nbins = 2048
samp_rate = 20
f_vec = np.linspace(-samp_rate/2.0,samp_rate/2.0,nbins)

context = zmq.Context()

#  Socket to talk to server
print("Connecting to RF spectrum data server")
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5557")

sess = tf.InteractiveSession()
ds = 16
f_vec_ds = np.linspace(-samp_rate/2.0,samp_rate/2.0,nbins/ds)
def max_pool_nx1(x,n):
  return tf.nn.max_pool(x, ksize=[1, n, 1, 1],
                        strides=[1, n, 1, 1], padding='SAME')   

x = tf.placeholder(tf.float32, shape=[None, nbins])

# Input Layer
x_image = tf.reshape(x, [-1,nbins,1,1])

with tf.name_scope("pool_stage") as scope:
  pool1 = max_pool_nx1(x_image,ds)

class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)
        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Buttons
        # New Data Button
        self.b1 = QtGui.QPushButton('Get New Data')
        self.b1.setObjectName("new_dat_but")
        self.b1.installEventFilter(self)

        # Append Data Button
        self.b2 = QtGui.QPushButton('Append Data')
        self.b2.setObjectName("app_dat_but")
        self.b2.installEventFilter(self)
        
        # Write Data Button
        self.b3 = QtGui.QPushButton('Write Data')
        self.b3.setObjectName("wr_dat_but")
        self.b3.installEventFilter(self)
        
        # Print Labels Button
        self.b4 = QtGui.QPushButton('Print Labels')
        self.b4.setObjectName("lab_print_but")
        self.b4.installEventFilter(self)

        # Print Data Size Button
        self.b5 = QtGui.QPushButton('Print Data Size')
        self.b5.setObjectName("print_print_but")
        self.b5.installEventFilter(self)

        # Load Old Data Button
        self.b6 = QtGui.QPushButton('Load Data')
        self.b6.setObjectName("load_dat_but")
        self.b6.installEventFilter(self)

        # Text boxes
        # Signal Label Text Box
        self.tx1 = QtGui.QLineEdit()
        self.tx1.setObjectName("sig_lab")
        self.tx1.installEventFilter(self)
        self.tx1_lab = QtGui.QLabel()
        self.tx1_lab.setText("Signal Label")
        
        # File Out Name
        self.tx2 = QtGui.QLineEdit()
        self.tx2.setObjectName("file_out")
        self.tx2.installEventFilter(self)
        self.tx2_lab = QtGui.QLabel()
        self.tx2_lab.setText("Write File Name")
        
        # Num Avg
        self.tx3 = QtGui.QLineEdit()
        self.tx3.setObjectName("num_avg")
        self.tx3.setValidator(QtGui.QIntValidator())
        self.tx3.installEventFilter(self)
        self.tx3_lab = QtGui.QLabel()
        self.tx3_lab.setText("Number of Averages")
        
        # RX Gain
        self.tx4 = QtGui.QLineEdit()
        self.tx4.setObjectName("gain_val")
        self.tx4.setValidator(QtGui.QDoubleValidator())
        self.tx4.installEventFilter(self)
        self.tx4_lab = QtGui.QLabel()
        self.tx4_lab.setText("RX Gain")

        # RX Freq
        self.tx5 = QtGui.QLineEdit()
        self.tx5.setObjectName("freq_val")
        self.tx5.setValidator(QtGui.QDoubleValidator())
        self.tx5.installEventFilter(self)
        self.tx5_lab = QtGui.QLabel()
        self.tx5_lab.setText("RX Freq")
        
        # Keyboard Control
        self.tx6 = QtGui.QLineEdit()
        self.tx6.setObjectName("key_cont")
        self.tx6.setValidator(QtGui.QDoubleValidator())
        self.tx6.installEventFilter(self)
        self.tx6_lab = QtGui.QLabel()
        self.tx6_lab.setText("Keyboard Control")

        # set the layout
        layout = QtGui.QGridLayout()

        layout.addWidget(self.toolbar,0,0,1,3)
        layout.addWidget(self.canvas,1,0,1,3)
        layout.addWidget(self.tx1_lab,2,0)
        layout.addWidget(self.tx1,2,1)
        layout.addWidget(self.tx2_lab,3,0)
        layout.addWidget(self.tx2,3,1)
        layout.addWidget(self.tx3_lab,4,0)
        layout.addWidget(self.tx3,4,1)
        layout.addWidget(self.tx4_lab,5,0)
        layout.addWidget(self.tx4,5,1)
        layout.addWidget(self.tx5_lab,6,0)
        layout.addWidget(self.tx5,6,1)
        layout.addWidget(self.tx6_lab,7,0)
        layout.addWidget(self.tx6,7,1)
        layout.addWidget(self.b1,2,2)
        layout.addWidget(self.b2,3,2)
        layout.addWidget(self.b3,4,2)
        layout.addWidget(self.b4,5,2)
        layout.addWidget(self.b5,6,2)
        layout.addWidget(self.b6,7,2)   
        
        self.setLayout(layout)

    def eventFilter(self, object, event):
        if ((event.type() == QtCore.QEvent.MouseButtonPress) and isinstance(object,QtGui.QPushButton)):
            if(object.objectName() == self.b1.objectName()):
                self.plot()
                return True
            
            if(object.objectName() == self.b2.objectName()):
                self.data_append()
                return True
            
            if(object.objectName() == self.b3.objectName()):
                self.data_write()
                return True

            if(object.objectName() == self.b4.objectName()):
                self.lab_print()
                return True
            
            if(object.objectName() == self.b5.objectName()):
                self.data_print()
                return True

            if(object.objectName() == self.b6.objectName()):
                self.data_load()
                return True

            else:
                return True
            
        if ((event.type() == QtCore.QEvent.KeyPress) and isinstance(object,QtGui.QLineEdit)):
            
            if(object.objectName() == self.tx6.objectName()):
                if(event.key()==QtCore.Qt.Key_A):
                    self.plot()
                    return True
                    
                if(event.key()==QtCore.Qt.Key_F):
                    self.data_append()
                    return True
                
                if(event.key()==QtCore.Qt.Key_S):
                    self.lab_print()
                    return True   
                   
                if(event.key()==QtCore.Qt.Key_D):
                    self.data_print()
                    return True                        
                    
                if(event.key()==QtCore.Qt.Key_Space):
                    self.data_write()
                    return True
                    
                if(event.key()==QtCore.Qt.Key_R):
                    self.data_load()
                    return True    
                        
            if(event.key()==QtCore.Qt.Key_Return):
                if(object.objectName() == self.tx1.objectName()):
                    self.button_labapp()
                    return True

                if(object.objectName() == self.tx3.objectName()):
                    self.set_avg()
                    return True                    

                if(object.objectName() == self.tx4.objectName()):
                    self.set_gain()
                    return True                    

                if(object.objectName() == self.tx5.objectName()):
                    self.set_freq()
                    return True                
                return True
            
        return False

    def enterPress(self):
        text = self.tx1.text()
        print "contents of text box: "+text

    def button_labapp(self):
        global lab_list
        text = self.tx1.text()
        if not(text in lab_list):
            lab_list.append(str(text))
            print("New Label: %s"%str(text))
        else:
            print("No New Label Detected")

    def lab_print(self):
        global lab_list
        #print(chr(27) + "[2J")
        print "Label Values are:"
        print lab_list

    def set_avg(self):
        global col_scan_str
        global col_scan_val
        global col_file
        avg_ind = col_scan_str.index("navg")
        val = self.tx3.text()
        val = int(val)
        col_scan_val[avg_ind] = val
        mlsdr.fecol_config.number_of_max_holds = col_scan_val[col_scan_str.index("navg")]
        print("Set number of avg. to: %d"%val)

    def set_gain(self):
        global rad_scan_str
        global rad_scan_val
        global rad_file
        gain_ind = rad_scan_str.index("gain")
        val = self.tx4.text()
        val = float(val)
        rad_scan_val[gain_ind] = val
        mlsdr.rad_config.gain = rad_scan_val[rad_scan_str.index("gain")]
        print("Retuned Gain to: %f"%val)

    def set_freq(self):
        global rad_scan_str
        global rad_scan_val
        global rad_file
        gain_ind = rad_scan_str.index("freq")
        val = self.tx5.text()
        val = float(val)
        rad_scan_val[gain_ind] = val
        mlsdr.rad_config.frequency = rad_scan_val[rad_scan_str.index("freq")]
        print("Retuned Freq to: %f"%val)

    def data_append(self):
        global dat_arr
        global lab_arr
        global lab_list
        global dat_mat_flat
        cur_lab = str(self.tx1.text())
        lab_ind = lab_list.index(cur_lab)
        if(dat_arr.shape[0]==0):
            dat_arr = np.zeros([1,dat_mat_flat.size])
            dat_arr[0,:] = dat_mat_flat
            
        else:
            dat_tmp = np.zeros([1,dat_mat_flat.size])
            dat_tmp[0,:] = dat_mat_flat
            dat_arr = np.append(dat_arr,dat_tmp,axis=0)
            
        if(lab_arr.shape[0]==0):
            lab_arr = np.zeros([1,len(lab_list)])
            lab_arr[0,lab_ind] = 1
            
        elif(len(lab_list)>lab_arr.shape[1]):
            labz_tmp = np.zeros([lab_arr.shape[0],len(lab_list)-lab_arr.shape[1]])
            lab_arr = np.append(lab_arr,labz_tmp,axis=1)
            lab_tmp = np.zeros([1,len(lab_list)])
            lab_tmp[0,lab_ind] = 1
            lab_arr = np.append(lab_arr,lab_tmp,axis=0)
            
        else:
            lab_tmp = np.zeros([1,len(lab_list)])
            lab_tmp[0,lab_ind] = 1
            lab_arr = np.append(lab_arr,lab_tmp,axis=0)
            
        print("Appended Data!")
            
    def data_print(self):
        global dat_arr
        global lab_arr
        global lab_list
        #print(chr(27) + "[2J")
        print("Current Data Size:")
        print dat_arr.shape        
        print("Current Label Size:")
        print lab_arr.shape
        print("Current Number of Samples:")
        print lab_list
        print np.sum(lab_arr,axis=0)

    def data_write(self):
        global data_dir
        global dat_arr
        global lab_arr
        global lab_list
        file_name = str(self.tx2.text())
        dat_name_out = data_dir+file_name+"_dat"
        lab_name_out = data_dir+file_name+"_lab"
        lab_list_out = data_dir+file_name+"_lab_list"
        with open(dat_name_out,'w') as dat_out:
            np.save(dat_out,dat_arr)

        with open(lab_name_out,'w') as lab_out:
            np.save(lab_out,lab_arr)
            
        with open(lab_list_out,'w') as labl_out:
            pickle.dump(lab_list,labl_out)    
            
        print("Data written to: %s"%dat_name_out)
        print("Labels written to: %s"%lab_name_out)
            
    def data_load(self):
        global dat_arr
        global lab_arr
        global lab_list
        file_name = str(self.tx2.text())
        dat_name_out = data_dir+file_name+"_dat"
        lab_name_out = data_dir+file_name+"_lab"
        lab_list_out = data_dir+file_name+"_lab_list"
        with open(dat_name_out,'r') as dat_out:
            dat_arr = np.load(dat_out)

        with open(lab_name_out,'r') as lab_out:
            lab_arr = np.load(lab_out)
            
        with open(lab_list_out,'r') as labl_out:
            lab_list = pickle.load(labl_out)
            
        print("Loaded Data!")          

    def plot(self):
        global ml_sdr
        global dat_mat_flat
        global seq_count
        # Time Stamp
        mlsdr.stamp.seq = seq_count
        cur_time = time.time()
        mlsdr.stamp.sec = int(np.floor(cur_time))
        mlsdr.stamp.nsec = int((cur_time-np.floor(cur_time))*1e9)
        message_out = mlsdr.SerializeToString()
        seq_count = seq_count+1
        print("Requesting data...")
        socket.send(message_out)
        message = socket.recv_multipart()
        print("Received data")
        dat_mat_flat = np.frombuffer(message[0],dtype = np.dtype('float64'))
        nvecs = dat_mat_flat.size/nbins
        dat_mat = np.reshape(dat_mat_flat,[nvecs,nbins],order = 'C')
        dat_mat_ds = sess.run(pool1,feed_dict={x: dat_mat})
        dat_mat_ds = np.reshape(dat_mat_ds,[nvecs,nbins/ds],order = 'C')
       
        # create an axis
        ax0 = self.figure.add_subplot(221)
        # discards the old graph
        ax0.hold(False)
        # plot data
        ax0.plot(f_vec,np.transpose(dat_mat))
        ax0.set_xlabel("Frequency in MHz")
        ax0.set_ylabel("Power in unscaled dB")
        ax0.set_title("Raw Data In")        
        
        # create an axis
        ax1 = self.figure.add_subplot(222)
        # discards the old graph
        ax1.hold(False)
        # plot data
        ax1.imshow(dat_mat)
        ax1.set_ylabel("Frequency Index")
        ax1.set_xlabel("Data Vector Index")
        ax1.set_title("Waterfall Plot")
        ax1.set_aspect('auto')
        
        # create an axis
        ax2 = self.figure.add_subplot(223)
        # discards the old graph
        ax2.hold(False)
        # plot data
        ax2.plot(f_vec_ds,np.transpose(dat_mat_ds))
        ax2.set_xlabel("Frequency in MHz")
        ax2.set_ylabel("Power in unscaled dB")
        ax2.set_title("Raw Data In") 
        
        # create an axis
        ax3 = self.figure.add_subplot(224)
        # discards the old graph
        ax3.hold(False)
        # plot data
        ax3.imshow(dat_mat_ds)
        ax3.set_ylabel("Frequency Index")
        ax3.set_xlabel("Data Vector Index")
        ax3.set_title("Waterfall Plot")
        ax3.set_aspect('auto')

        # refresh canvas
        self.canvas.draw()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())
