# Basic Testing of 2D CNN
import pickle
import tensorflow as tf
import numpy as np
import time
import matplotlib.pyplot as plt

data_dir = "Data_Files/ML_Data/"
file_name = "test2"
dat_name_out = data_dir+file_name+"_dat"
lab_name_out = data_dir+file_name+"_lab"
lab_list_out = data_dir+file_name+"_lab_list"

with open(dat_name_out,'r') as all_dat:
	input_data = np.load(all_dat)
	input_data = input_data.astype(np.float32)

with open(lab_name_out,'r') as all_lab:
	input_lab = np.load(all_lab)
	input_lab = input_lab.astype(np.float32)
	
with open(lab_list_out,'r') as labl_out:
    lab_list = pickle.load(labl_out)	

# 
print input_data.shape
print input_lab.shape
print lab_list
    
for jj in range(input_data.shape[0]):
    input_data[jj,:] = (input_data[jj,:]-np.min(input_data[jj,:]))*0.01

tot_len = input_lab.shape[0]
# randomizer
rand_ind = range(tot_len)
np.random.shuffle(rand_ind)
input_data = input_data[rand_ind,:]
input_lab = input_lab[rand_ind,:]

# Test/Train Split
tt_split = 0.5
train_len = int(np.round(tt_split*tot_len))
test_len = int(tot_len-train_len)
print train_len
print test_len
train_data = input_data[0:train_len,:]
train_lab = input_lab[0:train_len,:]
test_data = input_data[train_len:tot_len,:]
test_lab = input_lab[train_len:tot_len,:]
start_time = time.time()

ntrain = train_len
ntest = test_len

#train_data = (train_data+40.0)/80.0
#test_data = (test_data+40.0)/80.0

# Layer Variables
# Input
nbins = 2048
ds = 16
nvecs = 16
'''
# Small
# ReLU1
c1 = 5
h1 = 2
n1 = 16
# ReLU2
c2 = 3
h2 = 2
n2 = 32
# FC1
nfc1 = 1024
check_file = "Data_Files/ML_Checkpoints/2D_CNN_small.ckpt"
'''
# Big
# ReLU1
c1 = 7
h1 = 2
n1 = 32
# ReLU2
c2 = 5
h2 = 2
n2 = 64
# FC1
nfc1 = 2048
check_file = "Data_Files/ML_Checkpoints/2D_CNN_big.ckpt"
# FC2
flen = len(lab_list)
nfc2 = flen

sess = tf.InteractiveSession()

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.0001)
  #initial = tf.constant(0.0, shape=shape)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.0001, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_nxn(x,n):
  return tf.nn.max_pool(x, ksize=[1, n, n, 1],
                        strides=[1, n, n, 1], padding='SAME')                                            

def max_pool_1xn(x,n):
  return tf.nn.max_pool(x, ksize=[1, 1, n, 1],
                        strides=[1, 1, n, 1], padding='SAME') 

# Input/output placeholders
x = tf.placeholder(tf.float32, shape=[None, nbins*nvecs])
y_ = tf.placeholder(tf.float32, shape=[None, flen])

# Input Layer
x_image = tf.reshape(x, [-1,nvecs,nbins,1])

# First Layer: ReLU
with tf.name_scope("ReLU_1") as scope:
  pool_in = max_pool_1xn(x_image,ds)
  W_conv1 = weight_variable([c1, c1, 1, n1])
  b_conv1 = bias_variable([n1])
  h_conv1 = tf.nn.relu(conv2d(pool_in, W_conv1) + b_conv1)
  h_pool1 = max_pool_nxn(h_conv1,h1)

# Second Layer: ReLU
with tf.name_scope("ReLU_2") as scope:
  W_conv2 = weight_variable([c2, c2, n1, n2])
  b_conv2 = bias_variable([n2])
  h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
  h_pool2 = max_pool_nxn(h_conv2,h2)

# Third Layer: Dense
with tf.name_scope("Dense_1") as scope:
  W_fc1 = weight_variable([nbins/ds*nvecs/((h1*h1)*(h2*h2))*n2, nfc1])
  b_fc1 = bias_variable([nfc1])
  h_pool2_flat = tf.reshape(h_pool2, [-1, nbins/ds*nvecs/((h1*h1)*(h2*h2))*n2])
  h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
  keep_prob = tf.placeholder(tf.float32)
  h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# Readout Layer
with tf.name_scope("Readout_Layer") as scope:
  W_fc2 = weight_variable([nfc1, nfc2])
  b_fc2 = bias_variable([nfc2])
  y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2,name="y_conv")

with tf.name_scope("test") as scope:  
  correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
  accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
  accuracy_summary = tf.scalar_summary("accuracy", accuracy)

sess.run(tf.initialize_all_variables())
saver = tf.train.Saver()
saver.restore(sess, check_file)
print("Model restored!")

# Test Results
start_time = time.time()
print("test accuracy %g"%accuracy.eval(feed_dict={x: test_data, y_: test_lab, keep_prob: 1.0}))
time_diff = (time.time() - start_time)
print("test time: %s" % time_diff )
print("test msps: %s" % (test_data.size/time_diff/1e6))
print test_data.shape

# Train Results
start_time = time.time()
print("train accuracy %g"%accuracy.eval(feed_dict={x: train_data, y_: train_lab, keep_prob: 1.0}))
time_diff = (time.time() - start_time)
print("train time: %s" % time_diff )
print("train msps: %s" % (train_data.size/time_diff/1e6))
print train_data.shape

y_conv_out = sess.run(y_conv,feed_dict={x: test_data, y_: test_lab, keep_prob: 1.0})
print(y_conv_out.shape)
# Performance Plots
fax,axarr = plt.subplots(2,3)
sig_strs = ["Blank","ATSC","WIFI24","LTE","FM"]
ind_arr = [[0,0],[0,1],[0,2],[1,0],[1,1],[1,2]]
ind_arr0 = [0,0,0,1,1,1]
ind_arr1 = [0,1,2,0,1,2]
for ii in range(5):
  axarr[ind_arr0[ii],ind_arr1[ii]].plot(y_conv_out[test_lab[:,ii]==1,:])
  axarr[ind_arr0[ii],ind_arr1[ii]].set_title(sig_strs[ii])
  axarr[ind_arr0[ii],ind_arr1[ii]].set_xlabel("Test Data Index")
  axarr[ind_arr0[ii],ind_arr1[ii]].set_ylabel("Softmax Output")
  axarr[ind_arr0[ii],ind_arr1[ii]].set_ylim([-0.1,1.1])
  axarr[ind_arr0[ii],ind_arr1[ii]].set_xlim([0,y_conv_out[test_lab[:,ii]==1,:].shape[0]])
  axarr[ind_arr0[ii],ind_arr1[ii]].legend(sig_strs,loc=6)

# Confusion Matrix
conf_mat = np.zeros((5,5))
for ii in range(5):# True Label
  for jj in range(5):#Predicted Label 
    dum_vec = np.argmax(y_conv_out[test_lab[:,ii]==1,:],axis=1)
    one_vec = jj*np.ones(dum_vec.shape)
    dum_eq = np.equal(dum_vec,one_vec)
    dum_eq = dum_eq.astype(np.float32)
    conf_mat[ii,jj] = np.sum(dum_eq)

print(conf_mat.astype(int))
plt.show()



