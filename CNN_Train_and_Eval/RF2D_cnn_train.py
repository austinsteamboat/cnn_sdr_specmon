# Basic Training of 2D CNN
import pickle
import tensorflow as tf
import numpy as np
import time
import matplotlib.pyplot as plt

data_dir = "Data_Files/ML_Data/"
file_name = "test2"
dat_name_out = data_dir+file_name+"_dat"
lab_name_out = data_dir+file_name+"_lab"
lab_list_out = data_dir+file_name+"_lab_list"

with open(dat_name_out,'r') as all_dat:
	input_data = np.load(all_dat)
	input_data = input_data.astype(np.float32)

with open(lab_name_out,'r') as all_lab:
	input_lab = np.load(all_lab)
	input_lab = input_lab.astype(np.float32)
	
with open(lab_list_out,'r') as labl_out:
    lab_list = pickle.load(labl_out)	

# 
print input_data.shape
print input_lab.shape
print lab_list
    
for jj in range(input_data.shape[0]):
    input_data[jj,:] = (input_data[jj,:]-np.min(input_data[jj,:]))*0.01

tot_len = input_lab.shape[0]
# randomizer
rand_ind = range(tot_len)
np.random.shuffle(rand_ind)
input_data = input_data[rand_ind,:]
input_lab = input_lab[rand_ind,:]

# Test/Train Split
tt_split = 0.6
train_len = int(np.round(tt_split*tot_len))
test_len = int(tot_len-train_len)
print train_len
print test_len
train_data = input_data[0:train_len,:]
train_lab = input_lab[0:train_len,:]
test_data = input_data[train_len:tot_len,:]
test_lab = input_lab[train_len:tot_len,:]
start_time = time.time()

ntrain = train_len
ntest = test_len

#train_data = (train_data+40.0)/80.0
#test_data = (test_data+40.0)/80.0

# Layer Variables
# Input
nbins = 2048
ds = 16
nvecs = 16
# ReLU1
c1 = 5
h1 = 2
n1 = 16
# ReLU2
c2 = 3
h2 = 2
n2 = 32
# FC1
nfc1 = 1024
# FC2
flen = len(lab_list)
nfc2 = flen

sess = tf.InteractiveSession()

# Global Step
global_step = tf.Variable(0, name='global_step', trainable=False)
starter_learning_rate = 1e-3
learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
                                           100, 0.9, staircase=False)
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.0001)
  #initial = tf.constant(0.0, shape=shape)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.0001, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_nxn(x,n):
  return tf.nn.max_pool(x, ksize=[1, n, n, 1],
                        strides=[1, n, n, 1], padding='SAME')                                            

def max_pool_1xn(x,n):
  return tf.nn.max_pool(x, ksize=[1, 1, n, 1],
                        strides=[1, 1, n, 1], padding='SAME') 

# Input/output placeholders
x = tf.placeholder(tf.float32, shape=[None, nbins*nvecs])
y_ = tf.placeholder(tf.float32, shape=[None, flen])

# Input Layer
x_image = tf.reshape(x, [-1,nvecs,nbins,1])

# First Layer: ReLU
with tf.name_scope("ReLU_1") as scope:
  pool_in = max_pool_1xn(x_image,ds)
  W_conv1 = weight_variable([c1, c1, 1, n1])
  b_conv1 = bias_variable([n1])
  h_conv1 = tf.nn.relu(conv2d(pool_in, W_conv1) + b_conv1)
  h_pool1 = max_pool_nxn(h_conv1,h1)

# Second Layer: ReLU
with tf.name_scope("ReLU_2") as scope:
  W_conv2 = weight_variable([c2, c2, n1, n2])
  b_conv2 = bias_variable([n2])
  h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
  h_pool2 = max_pool_nxn(h_conv2,h2)

# Third Layer: Dense
with tf.name_scope("Dense_1") as scope:
  W_fc1 = weight_variable([nbins/ds*nvecs/((h1*h1)*(h2*h2))*n2, nfc1])
  b_fc1 = bias_variable([nfc1])
  h_pool2_flat = tf.reshape(h_pool2, [-1, nbins/ds*nvecs/((h1*h1)*(h2*h2))*n2])
  h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
  keep_prob = tf.placeholder(tf.float32)
  h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# Readout Layer
with tf.name_scope("Readout_Layer") as scope:
  W_fc2 = weight_variable([nfc1, nfc2])
  b_fc2 = bias_variable([nfc2])
  y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2,name="y_conv")

# Train and Evaluate
with tf.name_scope("xent") as scope:
  #cross_entropy = -tf.reduce_sum(y_*tf.log(y_conv))
  #cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(y_conv, labels)
  cross_entropy = -tf.reduce_sum(y_*tf.log(tf.clip_by_value(y_conv,1e-10,1.0)))
  #loss = tf.reduce_mean(cross_entropy)
  ce_summ = tf.scalar_summary("cross entropy", cross_entropy)

with tf.name_scope("train") as scope:
  train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy,global_step=global_step)
  
with tf.name_scope("test") as scope:  
  correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
  accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
  accuracy_summary = tf.scalar_summary("accuracy", accuracy)

# Merge all the summaries and write them out to /tmp/RFms_convnet
merged = tf.merge_all_summaries()
writer = tf.train.SummaryWriter("/tmp/RF2D_CNN", sess.graph)
#saver = tf.train.Saver()

saver = tf.train.Saver({"ReLU_1/Variable":W_conv1,
                        "ReLU_1/Variable_1":b_conv1,
                        "ReLU_2/Variable":W_conv2,
                        "ReLU_2/Variable_1":b_conv2,
                        "Dense_1/Variable":W_fc1,
                        "Dense_1/Variable_1":b_fc1,
                        "Readout_Layer/Variable":W_fc2,
                        "Readout_Layer/Variable_1":b_fc2})


sess.run(tf.initialize_all_variables())
debug1_out = sess.run(pool_in,feed_dict={x:test_data[0:1,:]})
print debug1_out.shape
fax,axarr = plt.subplots(2,1)
debug1_out = np.reshape(debug1_out,[16,128],order='C')
axarr[0].plot(np.transpose(debug1_out))
axarr[1].imshow(debug1_out)
plt.show()
'''
debug1_out = sess.run(h_conv1,feed_dict={x:test_data[0:10,:]})
debug2_out = sess.run(h_pool1,feed_dict={x:test_data[0:10,:]})
debug3_out = sess.run(h_conv2,feed_dict={x:test_data[0:10,:]})
debug4_out = sess.run(h_pool2,feed_dict={x:test_data[0:10,:]})
debug5_out = sess.run(h_pool2_flat,feed_dict={x:test_data[0:10,:]})
debug6_out = sess.run(h_fc1,feed_dict={x:test_data[0:10,:]})

print(debug1_out.shape)
print(debug2_out.shape)
print(debug3_out.shape)
print(debug4_out.shape)
print(debug5_out.shape)
print(debug6_out.shape)
'''
# Epoch Runs
batch_size = 300
for i in range(200):
  ii = i % (ntrain-batch_size)
  batch_xs = train_data[ii:(ii+batch_size),:]
  batch_ys = train_lab[ii:(ii+batch_size),:]
  if(i%10==0):
    global_step.assign(i)
    print('global_step: %s' % tf.train.global_step(sess,global_step))
    print('learning rate: %s' % sess.run(learning_rate))
    feed = {x:batch_xs, y_: batch_ys, keep_prob: 1.0}
    result = sess.run([merged, accuracy], feed_dict=feed)
    summary_str = result[0]
    acc = result[1]
    cxent = sess.run(cross_entropy,feed_dict=feed)
    print("step %d, training accuracy %g, cross_entropy %g"%(i, acc, cxent))
    print("Time since start: %s" % (time.time() - start_time))
    writer.add_summary(summary_str, i)
    y_conv_out = sess.run(y_conv,feed_dict=feed)
    # Confusion Matrix
    conf_mat = np.zeros((flen,flen))
    for ii in range(flen):# True Label
        for jj in range(flen):#Predicted Label 
          dum_vec = np.argmax(y_conv_out[batch_ys[:,ii]==1,:],axis=1)
          one_vec = jj*np.ones(dum_vec.shape)
          dum_eq = np.equal(dum_vec,one_vec)
          dum_eq = dum_eq.astype(np.float32)
          conf_mat[ii,jj] = np.sum(dum_eq)

    print(conf_mat.astype(int))
    
    
  train_step.run(feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 0.5})

save_path = saver.save(sess, "/tmp/RF2D_CNN/2D_CNN.ckpt")
print("Model saved in file: %s" % save_path)

print("Train time: %s" % (time.time() - start_time))
# Test Results
start_time = time.time()
print("test accuracy %g"%accuracy.eval(feed_dict={x: test_data, y_: test_lab, keep_prob: 1.0}))
time_diff = (time.time() - start_time)
print("test time: %s" % time_diff )
print("test msps: %s" % (test_data.size/time_diff/1e6))
print test_data.shape

y_conv_out = sess.run(y_conv,feed_dict={x:test_data,y_:test_lab,keep_prob:1.0})
# Confusion Matrix
conf_mat = np.zeros((flen,flen))
for ii in range(flen):# True Label
    for jj in range(flen):#Predicted Label 
      dum_vec = np.argmax(y_conv_out[test_lab[:,ii]==1,:],axis=1)
      one_vec = jj*np.ones(dum_vec.shape)
      dum_eq = np.equal(dum_vec,one_vec)
      dum_eq = dum_eq.astype(np.float32)
      conf_mat[ii,jj] = np.sum(dum_eq)

print(conf_mat.astype(int))          
          
